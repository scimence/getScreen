﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace sechduleGetScreen
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private ScreenParam getParams()
        {
            // 设置截屏控制信息
            ScreenParam param = new ScreenParam();

            if (!textBox1.Text.Trim().Equals("")) param.x = Int32.Parse(textBox1.Text.Trim());
            if (!textBox2.Text.Trim().Equals("")) param.y = Int32.Parse(textBox2.Text.Trim());
            if (!textBox3.Text.Trim().Equals("")) param.width = Int32.Parse(textBox3.Text.Trim());
            if (!textBox4.Text.Trim().Equals("")) param.height = Int32.Parse(textBox4.Text.Trim());

            if (!textBox5.Text.Trim().Equals("")) timer1.Interval = Int32.Parse(textBox5.Text.Trim());
            if (!textBox6.Text.Trim().Equals("")) param.delay_ms = Int32.Parse(textBox6.Text.Trim());
            
            return param;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            ScreenParam param = getParams();// 获取截屏控制信息
            ScreenShot.getScreen(param);    // 截屏
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!textBox5.Text.Trim().Equals("")) timer1.Interval = Int32.Parse(textBox5.Text.Trim());
            timer1.Start();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            timer1.Stop();
        }
    }
}

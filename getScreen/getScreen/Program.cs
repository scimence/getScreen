﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace getScreen
{
    static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// 通过参数调用截屏插件，call "%~dp0getScreen.exe" "D:\tmp\截屏4.png" 1000 0 0 -1 -1 true
        /// </summary>
        [STAThread]
        static void Main(String[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //Application.Run(new Form1());

            // 设置截屏控制信息
            ScreenParam param = new ScreenParam();
            param.programExit = true;               // 截屏执行完成后退出程序
            param.delay_ms = 5000;                  // 默认延时5秒截屏

            if (args.Length > 0)
            {
                if (args.Length > 0) param.savePath = args[0];                      // 保存路径
                if (args.Length > 1) param.delay_ms = Int32.Parse(args[1]);         // 延时（ms）
                if (args.Length > 5)
                {
                    param.x = Int32.Parse(args[2]);                                 // 截取区域
                    param.y = Int32.Parse(args[3]);
                    param.width = Int32.Parse(args[4]);
                    param.height = Int32.Parse(args[5]);
                }
                if (args.Length > 6) param.haveCursor = !args[5].Equals("false");   // 是否包含鼠标

                //MessageBox.Show(param.savePath + ", " + param.delay_ms + ", " + param.x + ", " + param.y + ", " + param.width + ", " + param.height + ", " + param.haveCursor); 
            }


            ScreenShot.getScreen(param);    // 截屏
            Application.Run();              // 在当前线程上运行应用程序消息循环
        }

        /// <summary>
        /// 调用系统退出
        /// </summary>
        public static void exit()
        {
            //Application.Exit();
            //Application.ExitThread();
            System.Environment.Exit(0);
        }
    }
}
